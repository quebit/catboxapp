import {StyleSheet} from 'react-native';

var Dimensions = require('Dimensions')
var {width, height} = Dimensions.get('window')

export default StyleSheet.create({
	bodyContainer: {
		flex: 1,
	},

	mainScrollView: {
		flex: 0.8,
	},

	main: {
		flex: 1,
		alignItems: 'center',
	},

	logo: {
		marginTop: height * 0.06,
		marginBottom: height * 0.05,
	},

	footer: {
		flex: 0.2, 
		backgroundColor: '#CCE8ED',
		alignItems: 'center',
	}
})
