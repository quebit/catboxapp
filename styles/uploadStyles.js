import {StyleSheet} from 'react-native';

var Dimensions = require('Dimensions')
var {width, height} = Dimensions.get('window')

export default StyleSheet.create({
  button: {
    backgroundColor: '#CCE8ED', 
    width: width * .6,
    borderRadius: 10,
  },

  largeText: {
    fontSize:24, 
    color: '#28538F', 
    marginTop: 30, 
    textAlign: 'center',
  },

  mediumText: {
    color: '#28538F', 
    textAlign: 'center', 
    paddingBottom: 10,
  },

  innerText: {
    fontSize:15, 
    padding: 5, 
    textAlign: 'left', 
    color: '#28538F',
  }
})
