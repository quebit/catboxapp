import {StyleSheet} from 'react-native';

var Dimensions = require('Dimensions')
var {width, height} = Dimensions.get('window')

export default StyleSheet.create({
	body: {
		width: width,
		flexDirection: "row",
		justifyContent: 'center',
		backgroundColor: '#CCE8ED',
		paddingTop: 0,
	},

	text: {
		color: '#28538F',
		padding: 5,
		fontSize: 18,
		textDecorationLine: 'underline',
		textDecorationStyle: 'double',
	},

	footer: {
		backgroundColor: '#CCE8ED',
		padding: 0,

	},
	
	footerText: {
		color: '#28538F',
		textAlign: 'left',
		fontSize: 15,
		paddingLeft: 15,
	},
})
