import {StyleSheet} from 'react-native';

var Dimensions = require('Dimensions')
var {width, height} = Dimensions.get('window')

export default StyleSheet.create({
	body: {
		marginTop: height * 0.05,
		marginBottom: height * 0.05,
	},

	button: { 
		backgroundColor: '#23B708',  
		width: width * .6,
		borderRadius: 10,
	},

	largeText: {
		fontSize: 20, 
		color: 'white', 
		textAlign: 'center', 
		paddingTop:15,
	},

	smallText: {
		textAlign: 'center', 
		color: 'white', 
		paddingTop:0, 
		paddingBottom: 5,
	}
})