import React, { Component } from 'react';
import {StyleSheet, TouchableOpacity, Text, View, Alert } from 'react-native';
import ImagePicker from 'react-native-image-picker';

import uploadStyles from '../../styles/uploadStyles';

export default class UploadButton extends Component {   
  render() {
  	return (
  		<View>
	    <TouchableOpacity onPress = {this.props.upload}  style = {uploadStyles.button}>
  		  <Text style = {uploadStyles.largeText}>upload</Text>
        <Text style = {uploadStyles.mediumText}>Select a file</Text>
        {this.props.precentLoaded && !this.props.link && (
          <View>
            <Text style = {uploadStyles.innerText}>%{this.props.precentLoaded} uploaded</Text>
          </View>  
        )} 
        {this.props.link && (
          <View>
            <Text style = {uploadStyles.innerText}>{this.props.name}</Text>
            <Text style = {uploadStyles.innerText}>{this.props.link}</Text>
          </View>
        )}
		  </TouchableOpacity>
      </View>
  	)}
}

