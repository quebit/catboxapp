import React, { Component } from 'react';
import {Linking,StyleSheet, TouchableOpacity, Text, View, Alert} from 'react-native';

import DisclaimerStyles from '../../styles/DisclaimerStyles';

export default class Disclaimer extends Component {
  render() {
    return (
      <View style={{flex: 1, marginTop: 20, marginBottom:20}}>
        <View style={DisclaimerStyles.body}>
          <Text onPress={() => {Linking.openURL("https://catbox.moe/legal.php");}} style={DisclaimerStyles.text}>
           Legal/Privacy Policy 
          </Text>
          <Text style={DisclaimerStyles.text}>|</Text>
          <Text onPress={() => {Linking.openURL("https://catbox.moe/faq.php");}} style={DisclaimerStyles.text}>
           FAQ 
          </Text>
          <Text style={DisclaimerStyles.text}>|</Text>
          <Text onPress={() => {Linking.openURL("https://www.patreon.com/catbox");}} style={DisclaimerStyles.text}>
           Patreon 
          </Text>
        </View>
        <View style={DisclaimerStyles.footer}>
          <Text style={DisclaimerStyles.footerText}>I am not affiliated with catbox.moe </Text>
          <Text style={DisclaimerStyles.footerText}>I just made the app. </Text>
        </View>
      </View>
    );
  }
}

