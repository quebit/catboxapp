import React, { Component } from 'react';
import {StyleSheet, Clipboard, TouchableOpacity, Text, View} from 'react-native';

import copyStyles from '../../styles/copyStyles';

export default class CopyButton extends Component {
  constructor(props) {
    super(props);
    this.state = { pressed: false };
  }

  copyToClipboard = async () => {
    await Clipboard.setString(this.props.link)
    this.setState({pressed: true })   
    setTimeout(() => {this.setState({pressed: false})}, 1000)
  }

  render() {
    return (
      <View style={copyStyles.body}>
        <TouchableOpacity onPress = {this.copyToClipboard} style = {copyStyles.button}>
          <Text style = {copyStyles.largeText}>copy</Text>
          <Text style = {copyStyles.smallText}>Copy link</Text>
          {this.state.pressed && (
            <View>
              <Text style = {copyStyles.smallText}>Copied text!</Text>
            </View>
          )}	
        </TouchableOpacity>
      </View>
    );
  }
}
