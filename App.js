import React, { Component } from 'react';
import {ScrollView, Image, View, Text, Alert} from 'react-native';
import ImagePicker from 'react-native-image-picker';

import UploadButton from './src/components/UploadButton';
import CopyButton from './src/components/CopyButton';
import Disclaimer from './src/components/Disclaimer';

import indexStyles from './styles/index.js';

export default class CatboxApp extends Component {
  constructor(props) {
    super(props);
    this.state = { link: null, name: null, precentLoaded: null };
  }
  upload = () =>  {
    var options = {};
    ImagePicker.showImagePicker(options, (file) => {
      if (!file.didCancel) {
        this.setState({name: file.fileName}) 
        var DataToSend = new FormData()
        DataToSend.append('reqtype', 'fileupload')
        DataToSend.append('fileToUpload', {
          uri: file.uri, 
          type: 'image/png',
          name: file.fileName
        })
        var request = new XMLHttpRequest()
        request.onreadystatechange = () => {
          if (request.readyState === XMLHttpRequest.DONE) {
            if (request.status === 200) {
              this.setState({link: request.response }) 
            }
            else {
              Alert.alert('something went wrong!')
              Alert.alert(request.response)
            } 
          }
        }
        request.upload.onprogress = ('progress', (evt) => {
          if (evt.lengthComputable) {
            var percentComplete = (evt.loaded / evt.total) * 100
            percentComplete = +percentComplete.toFixed(2)
            this.setState({precentLoaded: percentComplete })
          }
        })
        request.open('POST', 'https://catbox.moe/user/api.php'); 
        request.send(DataToSend);
      } //end of cancel
    }) //end of imagepicker
  }; //end of upload

  render() {
    return (
      <View style= {indexStyles.bodyContainer}>
        <ScrollView style= {indexStyles.mainScrollView}>
         <View style = {indexStyles.main}> 
            <Image style = {indexStyles.logo} source = {require('./media/img/logo_fixed_font.png')}/>
            <UploadButton name = {this.state.name} link = {this.state.link} upload = {this.upload} precentLoaded = {this.state.precentLoaded}/>
            {this.state.link && (
               <View>
                <CopyButton link = {this.state.link} />  
              </View>
            )}
          </View> 
        </ScrollView>
        <View style={indexStyles.footer}>
          <Disclaimer/>
        </View>
      </View>
    );
  }
}
